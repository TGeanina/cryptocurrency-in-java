**Cryptocurrency** is an application that reads, stores, and output data on cryptocurrency data. Cryptocurrency reads through a simplified TXT file, stores the data from each coin entry in a structured way (e.g., the list of coins is stored as an array of coins with one array element per name), and provides output derived from the input (e.g., besides listing the names of the coins, Cryptocurrency indicates the number of observations on that publication).
The purpose of this project was to create a simple system to read, search, remove and write bibliographic data from TXT file.


**Objectives**: use standard Java I/O to read, write and user interaction; develop and use at least six classes to model different types of cryptocurrency data; encapsulate all primitive arrays inside classes that provide controlled access to the array data and retain information on array capacity and use; integrate appropriate exception handling into classes that encapsulate arrays; store the coin entries in a data structure that varies in size as elements are inserted and removed; use binary search to insert coin entries into and remove coins entries from an initially empty data structure; develop and use an appropriate design; use proper documentation and formatting.

The program reads the input from a TXT file called “coin.txt” which is provided by the “coinmarketcap.com”. 

After reading in and storing all entries in the file, Cryptocurrency enters a loop where it prompts the user with six options: ‘S’ for search, ‘D’ for remove, ‘W’ for print, M for merge, P for purge, and ‘E’ for the exit. .
Furthermore, each class has an appropriate constructor, display, and size methods. The size methods indicate how many items are stored in a given array. If a field is null, then it is not counted for the number of fields. 
