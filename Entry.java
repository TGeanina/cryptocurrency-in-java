/**
 * @author Geanina Tambaliuc
 * @param <T>
 *
 */
public class Entry extends Count implements Comparable<Entry> {
	protected String id, name, symbol, rank,priceUsd, priceBtc, volume24h, marketCap,availableSup;
	protected String totalSup, maxSup, percent1, percent24, percent7d, lastUpdated;
	
	
	/**
	 * empty constructor
	 */
	public Entry()
	{
		
	}
	/** Constructor for entry (includes the fields for count)
	 * @param id
	 * @param name
	 * @param symbol
	 * @param rank
	 * @param priceUsd
	 * @param priceBtc
	 * @param volume24h
	 * @param marketCap
	 * @param availableSup
	 * @param totalSup
	 * @param maxSup
	 * @param percent1
	 * @param percent24
	 * @param percent7d
	 * @param lastUpdated
	 *  @param fields
	 * @param lossP
	 * @param longest
	 * @param length
	 * @param profit
	 */
	public Entry(String id, String name, String symbol, String rank, String priceUsd, String priceBtc, String volume24h,
			String marketCap, String availableSup, String totalSup, String maxSup, String percent1, String percent24,
			String percent7d, String lastUpdated,int fields, float lossP, String longest, int length, float profit) {
		super(fields, lossP, longest, length, profit);
		this.id = id;
		this.name = name;
		this.symbol = symbol;
		this.rank = rank;
		this.priceUsd = priceUsd;
		this.priceBtc = priceBtc;
		this.volume24h = volume24h;
		this.marketCap = marketCap;
		this.availableSup = availableSup;
		this.totalSup = totalSup;
		this.maxSup = maxSup;
		this.percent1 = percent1;
		this.percent24 = percent24;
		this.percent7d = percent7d;
		this.lastUpdated = lastUpdated;
	}
	

	/** Constructor for entry
	 * @param id
	 * @param name
	 * @param symbol
	 * @param rank
	 * @param priceUsd
	 * @param priceBtc
	 * @param volume24h
	 * @param marketCap
	 * @param availableSup
	 * @param totalSup
	 * @param maxSup
	 * @param percent1
	 * @param percent24
	 * @param percent7d
	 * @param lastUpdated
	 */
	public Entry(String id, String name,String symbol, String rank, String priceUsd, String priceBtc, String volume24h, String marketCap,
			String availableSup, String totalSup, String maxSup, String percent1, String percent24, String percent7d,
			String lastUpdated) {
		this.id = id;
		this.name = name;
		this.symbol = symbol;
		this.rank = rank;
		this.priceUsd = priceUsd;
		this.priceBtc = priceBtc;
		this.volume24h = volume24h;
		this.marketCap = marketCap;
		this.availableSup = availableSup;
		this.totalSup = totalSup;
		this.maxSup = maxSup;
		this.percent1 = percent1;
		this.percent24 = percent24;
		this.percent7d = percent7d;
		this.lastUpdated = lastUpdated;
	}


	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the symbol
	 */
	public String getSymbol() {
		return symbol;
	}
	/**
	 * @param symbol the symbol to set
	 */
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	/**
	 * @return the rank
	 */
	public String getRank() {
		return rank;
	}
	/**
	 * @param rank the rank to set
	 */
	public void setRank(String rank) {
		this.rank = rank;
	}
	/**
	 * @return the priceUsd
	 */
	public String getPriceUsd() {
		return priceUsd;
	}
	/**
	 * @param priceUsd the priceUsd to set
	 */
	public void setPriceUsd(String priceUsd) {
		this.priceUsd = priceUsd;
	}
	/**
	 * @return the priceBtc
	 */
	public String getPriceBtc() {
		return priceBtc;
	}
	/**
	 * @param priceBtc the priceBtc to set
	 */
	public void setPriceBtc(String priceBtc) {
		this.priceBtc = priceBtc;
	}
	/**
	 * @return the volume24h
	 */
	public String getVolume24h() {
		return volume24h;
	}
	/**
	 * @param volume24h the volume24h to set
	 */
	public void setVolume24h(String volume24h) {
		this.volume24h = volume24h;
	}
	/**
	 * @return the marketCap
	 */
	public String getMarketCap() {
		return marketCap;
	}
	/**
	 * @param marketCap the marketCap to set
	 */
	public void setMarketCap(String marketCap) {
		this.marketCap = marketCap;
	}
	/**
	 * @return the availableSup
	 */
	public String getAvailableSup() {
		return availableSup;
	}
	/**
	 * @param availableSup the availableSup to set
	 */
	public void setAvailableSup(String availableSup) {
		this.availableSup = availableSup;
	}
	/**
	 * @return the totalSup
	 */
	public String getTotalSup() {
		return totalSup;
	}
	/**
	 * @param totalSup the totalSup to set
	 */
	public void setTotalSup(String totalSup) {
		this.totalSup = totalSup;
	}
	/**
	 * @return the maxSup
	 */
	public String getMaxSup() {
		return maxSup;
	}
	/**
	 * @param maxSup the maxSup to set
	 */
	public void setMaxSup(String maxSup) {
		this.maxSup = maxSup;
	}
	/**
	 * @return the percent1
	 */
	public String getPercent1() {
		return percent1;
	}
	/**
	 * @param percent1 the percent1 to set
	 */
	public void setPercent1(String percent1) {
		this.percent1 = percent1;
	}
	/**
	 * @return the percent24
	 */
	public String getPercent24() {
		return percent24;
	}
	/**
	 * @param percent24 the percent24 to set
	 */
	public void setPercent24(String percent24) {
		this.percent24 = percent24;
	}
	/**
	 * @return the percent7d
	 */
	public String getPercent7d() {
		return percent7d;
	}
	/**
	 * @param percent7d the percent7d to set
	 */
	public void setPercent7d(String percent7d) {
		this.percent7d = percent7d;
	}
	/**
	 * @return the lastUpdated
	 */
	public String getLastUpdated() {
		return lastUpdated;
	}
	/**
	 * @param lastUpdated the lastUpdated to set
	 */
	public void setLastUpdated(String lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
    /**
     * @return str which contains the elements of the entry
	*/
	@Override
	public String toString()
	{
		String str="        \"id\": "+this.getId()+"\r\n";
		str=str+"        \"name\": "+this.getName()+"\r\n";
		str=str+"        \"symbol\": "+this.getSymbol()+"\r\n";
		str=str+"        \"rank\": "+this.getRank()+"\r\n";
		str=str+"        \"price_usd\": "+this.getPriceUsd()+"\r\n";
		str=str+"        \"price_btc\": "+this.getPriceBtc()+"\r\n";
		str=str+"        \"24h_volume_usd\": "+this.getVolume24h()+"\r\n";
		str=str+"        \"market_cap_usd\": "+this.getMarketCap()+"\r\n";
		str=str+"        \"available_supply\": "+this.getAvailableSup()+"\r\n";
		str=str+"        \"total_supply\": "+this.getTotalSup()+"\r\n";
		str=str+"        \"max_supply\": "+this.getMaxSup()+"\r\n";
		str=str+"        \"percent_change_1h\": "+this.getPercent1()+"\r\n";
		str=str+"        \"percent_change_24h\": "+this.getPercent24()+"\r\n";
		str=str+"        \"percent_change_7d\": "+this.getPercent7d()+"\r\n";
		str=str+"        \"last_updated\": "+"\""+this.getLastUpdated()+"\""+"\r\n";
	    str=str+"        \"counts\":  \"fields=" +this.fields+"\"";
		if(this.lossP>0)
			str=str+", \"profit="+this.lossP+"\",";
		else if(this.lossP<0)
			str=str+", \"loss="+-this.lossP+"\",";
		else str=str+", \"loss/profit=null\""+"\",";
		str=str+" \"longest="+this.longest+ ", \"length="+this.length+"\""+","+"\n";
		return str;
	}
	
	/**
	 * @return fields - the size of an entry
	 */
	public int sizeEntry()
	{
		return fields;
	}
	
	/** 
	 * overrides the compareTo method 
	 * compares by name
	 */
	@Override
	public int compareTo(Entry entry) {
		// TODO Auto-generated method stub
		return this.name.compareTo(entry.name);
	}
	
	
}
