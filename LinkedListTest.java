
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.Test;

class LinkedListTest {

	/**
	 * test for adding elements to the list by name, maintaining order
	 * Also, test for newNode and printList method
	 * @throws ListEmptyException
	 */
	@Test
	void testSortedInsertName() throws ListEmptyException {
		Entry e1=new Entry("23","bitcoin","btc","null","null","null","null","null","null","null","356","null","null","null","null");
		Entry e2=new Entry("4","aus","a","null","null","null","null","null","null","null","245","null","null","null","null");
		Entry e3=new Entry("7","opsd","op","null","null","null","null","null","null","null","123","null","null","null","null");
		Entry e4=new Entry("35","lasd","l","null","null","null","null","null","null","null","376","null","null","null","null");
		
		MyDoubleLinkedList list=new MyDoubleLinkedList();
		Node new_Node=list.newNode(e1, e1.name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase(),e1.name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
		list.sortedInsert(new_Node);
		 new_Node=list.newNode(e2, e2.name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase(),e2.name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
		list.sortedInsert(new_Node);
		 new_Node=list.newNode(e3, e3.name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase(),e3.name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
		list.sortedInsert(new_Node);
		new_Node=list.newNode(e4, e4.name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase(),e4.name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
		
		
		
		list.sortedInsert(new_Node);
		list.printList();
	}
	
	/**
	 * test for adding elements to the list by symbol, maintaining order
	 * 
	 * @throws ListEmptyException
	 */
	@Test
	void testSortedInsertSymbol() throws ListEmptyException {
		Entry e1=new Entry("23","bitcoin","btc","null","null","null","null","null","null","null","356","null","null","null","null");
		Entry e2=new Entry("4","aus","a","null","null","null","null","null","null","null","245","null","null","null","null");
		Entry e3=new Entry("7","opsd","op","null","null","null","null","null","null","null","123","null","null","null","null");
		Entry e4=new Entry("35","lasd","l","null","null","null","null","null","null","null","376","null","null","null","null");
		
		MyDoubleLinkedList list=new MyDoubleLinkedList();
		Node new_Node=list.newNode(e1, e1.symbol.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase(),e1.name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
		list.sortedInsert(new_Node);
		 new_Node=list.newNode(e2, e2.symbol.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase(),e2.name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
		list.sortedInsert(new_Node);
		 new_Node=list.newNode(e3, e3.symbol.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase(),e3.name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
		list.sortedInsert(new_Node);
		new_Node=list.newNode(e4, e4.symbol.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase(),e4.name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
		list.sortedInsert(new_Node);
		list.printList();
	}
	
	/**
	 * test for adding elements to the list by max_Supply, maintaining order
	 * @throws ListEmptyException
	 */
	@Test
	void testSortedInsertMaxSup() throws ListEmptyException {
		Entry e1=new Entry("23","bitcoin","btc","null","null","null","null","null","null","null","356","null","null","null","null");
		Entry e2=new Entry("4","aus","a","null","null","null","null","null","null","null","245","null","null","null","null");
		Entry e3=new Entry("7","opsd","op","null","null","null","null","null","null","null","123","null","null","null","null");
		Entry e4=new Entry("35","lasd","l","null","null","null","null","null","null","null","376","null","null","null","null");
		
		MyDoubleLinkedList list=new MyDoubleLinkedList();
		Node new_Node=list.newNode(e1, e1.maxSup.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase(),e1.name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
		list.sortedInsert(new_Node);
		 new_Node=list.newNode(e2, e2.maxSup.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase(),e2.name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
		list.sortedInsert(new_Node);
		 new_Node=list.newNode(e3, e3.maxSup.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase(),e3.name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
		list.sortedInsert(new_Node);
		new_Node=list.newNode(e4, e4.maxSup.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase(),e4.name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
		list.sortedInsert(new_Node);
		list.printList();
	}
	
	/**
	 * test for size method
	 * @throws ListEmptyException
	 */
	@Test
	void testSize() throws ListEmptyException {
		Entry e1=new Entry("23","bitcoin","btc","null","null","null","null","null","null","null","356","null","null","null","null");
		Entry e2=new Entry("4","aus","a","null","null","null","null","null","null","null","245","null","null","null","null");
		Entry e3=new Entry("7","opsd","op","null","null","null","null","null","null","null","123","null","null","null","null");
		Entry e4=new Entry("35","lasd","l","null","null","null","null","null","null","null","376","null","null","null","null");
		
		MyDoubleLinkedList list=new MyDoubleLinkedList();
		Node new_Node=list.newNode(e1, e1.symbol.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase(),e1.name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
		list.sortedInsert(new_Node);
		 new_Node=list.newNode(e2, e2.symbol.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase(),e2.name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
		list.sortedInsert(new_Node);
		 new_Node=list.newNode(e3, e3.symbol.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase(),e3.name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
		list.sortedInsert(new_Node);
		new_Node=list.newNode(e4, e4.symbol.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase(),e4.name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
		list.sortedInsert(new_Node);
		
		assertTrue(list.getSize()==4);
	}
	/**
	 * test for getHead method
	 * @throws ListEmptyException
	 */
	@Test
	void testHead() throws ListEmptyException {
		Entry e1=new Entry("23","bitcoin","btc","null","null","null","null","null","null","null","356","null","null","null","null");
		Entry e2=new Entry("4","aus","a","null","null","null","null","null","null","null","245","null","null","null","null");
		Entry e3=new Entry("7","opsd","op","null","null","null","null","null","null","null","123","null","null","null","null");
		Entry e4=new Entry("35","lasd","l","null","null","null","null","null","null","null","376","null","null","null","null");
		
		MyDoubleLinkedList list=new MyDoubleLinkedList();
		Node new_Node=list.newNode(e1, e1.symbol.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase(),e1.name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
		list.sortedInsert(new_Node);
		 new_Node=list.newNode(e2, e2.symbol.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase(),e2.name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
		list.sortedInsert(new_Node);
		 new_Node=list.newNode(e3, e3.symbol.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase(),e3.name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
		list.sortedInsert(new_Node);
		new_Node=list.newNode(e4, e4.symbol.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase(),e4.name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
		list.sortedInsert(new_Node);
		
		assertTrue(list.getHead().name=="aus");
	}
	
	/**
	 * test for adding at the end of the list
	 * @throws ListEmptyException
	 */
	@Test
	void testAddEnd() throws ListEmptyException {
		Entry e1=new Entry("23","bitcoin","btc","null","null","null","null","null","null","null","356","null","null","null","null");
		Entry e2=new Entry("4","aus","a","null","null","null","null","null","null","null","245","null","null","null","null");
		Entry e3=new Entry("7","opsd","op","null","null","null","null","null","null","null","123","null","null","null","null");
		Entry e4=new Entry("35","lasd","l","null","null","null","null","null","null","null","376","null","null","null","null");
		
		MyDoubleLinkedList list=new MyDoubleLinkedList();
		Node new_Node=list.newNode(e1, e1.symbol.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase(),e1.name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
		list.addEnd(new_Node);
		 new_Node=list.newNode(e2, e2.symbol.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase(),e2.name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
		list.addEnd(new_Node);
		 new_Node=list.newNode(e3, e3.symbol.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase(),e3.name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
		list.addEnd(new_Node);
		new_Node=list.newNode(e4, e4.symbol.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase(),e4.name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
		list.addEnd(new_Node);
		
		list.printList();
	}
	
	/**
	 * test for searching a specific node
	 * @throws ListEmptyException
	 */
	@Test
	void testSearch() throws ListEmptyException {
		Entry e1=new Entry("23","bitcoin","btc","null","null","null","null","null","null","null","356","null","null","null","null");
		Entry e2=new Entry("4","aus","a","null","null","null","null","null","null","null","245","null","null","null","null");
		Entry e3=new Entry("7","opsd","op","null","null","null","null","null","null","null","123","null","null","null","null");
		Entry e4=new Entry("35","lasd","l","null","null","null","null","null","null","null","376","null","null","null","null");
		
		MyDoubleLinkedList list=new MyDoubleLinkedList();
		Node new_Node=list.newNode(e1, e1.symbol.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase(),e1.name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
		list.sortedInsert(new_Node);
		 new_Node=list.newNode(e2, e2.symbol.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase(),e2.name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
		list.sortedInsert(new_Node);
		 new_Node=list.newNode(e3, e3.symbol.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase(),e3.name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
		list.addEnd(new_Node);
		new_Node=list.newNode(e4, e4.symbol.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase(),e4.name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
		list.sortedInsert(new_Node);
		
		assertTrue(list.search("bitcoin")!=-1);
	}
	/**
	 * test for the isEmpty method
	 */
	@Test
	void testIsEmpty()
	{
	
		Entry e1=new Entry("23","bitcoin","btc","null","null","null","null","null","null","null","356","null","null","null","null");
		Entry e2=new Entry("4","aus","a","null","null","null","null","null","null","null","245","null","null","null","null");
		Entry e3=new Entry("7","opsd","op","null","null","null","null","null","null","null","123","null","null","null","null");
		Entry e4=new Entry("35","lasd","l","null","null","null","null","null","null","null","376","null","null","null","null");
		
		MyDoubleLinkedList list=new MyDoubleLinkedList();
		Node new_Node=list.newNode(e1, e1.name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase(),e1.name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
		list.sortedInsert(new_Node);
		 new_Node=list.newNode(e2, e2.name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase(),e2.name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
		list.sortedInsert(new_Node);
		 new_Node=list.newNode(e3, e3.name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase(),e3.name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
		list.sortedInsert(new_Node);
		new_Node=list.newNode(e4, e4.name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase(),e4.name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
		
		list.sortedInsert(new_Node);
		assertTrue(!list.isEmpty());
	}
	
}
