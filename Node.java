
/**
 * @author Geanina Tambaliuc
 * class for nodes
 *
 */
public class Node {
	    protected Object data;
	    protected Node next, prev;
	    protected String comp;
	    protected float comp2;
	    protected String name;
	 
	    /**
	     * Empty constructor
	     */
	    public Node()
	    {
	    	
	    }
	   
	    
	    /**
	     * Constructor
	     * @param d - data
	     * @param comparator - comparator
	     * @param name - Node's name
	     */
	    public Node(Object d,String comparator,String name)
	    {
	        next = null;
	        prev = null;
	        data = d;
	        comp=comparator;
	        this.name=name;
	    }
	    
	    /**
	     * Constructor
	     * @param d - data
	     * @param comparator - comparator
	     * @param name - Node's name
	     */
	    public Node(Object d,float comparator,String name)
	    {
	        next = null;
	        prev = null;
	        data = d;
	        comp2=comparator;
	        this.name=name;
	    }
	    /**
		 * @return the next
		 */
		public Node getNext() {
			return next;
		}
		/**
		 * @param next the next to set
		 */
		public void setNext(Node next) {
			this.next = next;
		}
		/**
		 * @return the prev
		 */
		public Node getPrev() {
			return prev;
		}
		/**
		 * @param prev the prev to set
		 */
		public void setPrev(Node prev) {
			this.prev = prev;
		}
		/**
		 * @return the comp
		 */
		public String getComp() {
			return comp;
		}
		/**
		 * @param comp the comp to set
		 */
		public void setComp(String comp) {
			this.comp = comp;
		}
		/**
		 * @return the comp2
		 */
		public float getComp2() {
			return comp2;
		}
		/**
		 * @param comp2 the comp2 to set
		 */
		public void setComp2(float comp2) {
			this.comp2 = comp2;
		}
		/**
		 * @return the name
		 */
		public String getName() {
			return name;
		}
		/**
		 * @param name the name to set
		 */
		public void setName(String name) {
			this.name = name;
		}
		/**
		 * @param data the data to set
		 */
		public void setData(Object data) {
			this.data = data;
		}
		
		
	    /**
	     * Set the next of the node
	     * @param n
	     */
	    public void setLinkNext(Node n)
	    {
	        next = n;
	    }
	    
	    /**
	     * Set the prev of the node
	     * @param n
	     */
	    public void setLinkPrev(Node p)
	    {
	        prev = p;
	    }    
	    
	    
	    /**
	     * Get next node
	     * @return next Node
	     */
	    public Node getLinkNext()
	    {
	        return next;
	    }
	    
	    
	   
	    /**
	     * Get the previous node
	     * @return prev
	     */
	    public Node getLinkPrev()
	    {
	        return prev;
	    }
	
	    
	    /**
	     * Get the data of a node
	     * @return data
	     */
	    public Object getData()
	    {
	        return data;
	    }
	    
	    /**
	     * Display Node's data
	     */
	    public void displayNode() {
	           System.out.print( data + " ");
	    }
}
