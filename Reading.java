import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.Scanner;

/**
 * @author Geanina Tambaliuc
 * Reads the file and stores the entries
 *
 */
public class Reading extends LossProfit {
	
	protected static int count;
	protected static Entry[] entries;
	protected static MyDoubleLinkedList list=new MyDoubleLinkedList();
	
	/**
	 * Reads the file and stores the entries
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws NegativeNumberException
	 * @throws DivisionBy0
	 */
	public static void reading(String comp, String filename) throws FileNotFoundException, IOException, NegativeNumberException, DivisionBy0
	{
		
		Node new_Node;
		Scanner scan2=new Scanner(new File(filename)); // reads the file coins.txt
	    
		// create an array of objects, type Entry
		entries= new Entry[5];
		for(int i=0;i<entries.length;i++)
		{
			entries[i]=new Entry(); //add element in the array
		}
		String temp;
		count=0;
		int maxim=0;
		while(scan2.hasNext())
		{
			
			if(scan2.nextLine().equals("    {"))
			{
				temp=scan2.nextLine();		// scan file line
				while(temp.trim().equals("")) // checks if the line is empty
				{ 
					temp=scan2.nextLine();
				}
				String [] id=temp.split(":"); // splits the line in 2 sections
				entries[count].setId(id[1]);	
				if(size(id[1])>maxim)   // calculate the maximum field
				{
					
					maxim=size(id[1]);
					entries[count].setLength(maxim);
					entries[count].setLongest(id[0].replaceAll("\\s+",""));
				}
				if(!id[1].replace("\"","").replaceAll("\\s+","").replace(",","").equals("null") ) // count fields
				entries[count].setFields(entries[count].getFields()+1);
				
				temp=scan2.nextLine();
				while(temp.trim().equals("")) // checks if the line is empty
				{ 
					temp=scan2.nextLine();
				}
				String [] name=temp.split(":");
				entries[count].setName(name[1]);
				if(size(name[1])>maxim)
				{
					maxim=size(name[1]);
					entries[count].setLength(maxim);
					entries[count].setLongest(name[0].replaceAll("\\s+",""));
					
				}
				if(!name[1].replace("\"","").replaceAll("\\s+","").replace(",","").equals("null"))
				entries[count].setFields(entries[count].getFields()+1);
				
				
				temp=scan2.nextLine();
				while(temp.trim().equals("")) // checks if the line is empty
				{ 
					temp=scan2.nextLine();
				}
				String [] symbol=temp.split(":");
				entries[count].setSymbol(symbol[1]);
				if(size(symbol[1])>maxim)
				{
					maxim=size(symbol[1]);
					entries[count].setLength(maxim);
					entries[count].setLongest(symbol[0].replaceAll("\\s+",""));
				}
				if(!symbol[1].replace("\"","").replaceAll("\\s+","").replace(",","").equals("null"))
				entries[count].setFields(entries[count].getFields()+1);
				
				
				temp=scan2.nextLine();
				while(temp.trim().equals("")) // checks if the line is empty
				{ 
					temp=scan2.nextLine();
				}
				String [] rank=temp.split(":");
				entries[count].setRank((rank[1]));
				if(size(rank[1])>maxim)
				{
					maxim=size(rank[1]);
					entries[count].setLength(maxim);
					entries[count].setLongest(rank[0].replaceAll("\\s+",""));
				}
				if(!rank[1].replace("\"","").replaceAll("\\s+","").replace(",","").equals("null"))
				entries[count].setFields(entries[count].getFields()+1);
				
				
				temp=scan2.nextLine();
				while(temp.trim().equals("")) // checks if the line is empty
				{ 
					temp=scan2.nextLine();
				}
				String [] priceUsd=temp.split(":");
				entries[count].setPriceUsd((priceUsd[1]));
				if(size(priceUsd[1])>maxim)
				{
					maxim=size(priceUsd[1]);
					entries[count].setLength(maxim);
					entries[count].setLongest(priceUsd[0].replaceAll("\\s+",""));
				}
				if(!priceUsd[1].replace("\"","").replaceAll("\\s+","").replace(",","").equals("null"))
				entries[count].setFields(entries[count].getFields()+1);
				
				
				temp=scan2.nextLine();
				while(temp.trim().equals("")) // checks if the line is empty
				{ 
					temp=scan2.nextLine();
				}
				String [] priceBtc=temp.split(":");
				entries[count].setPriceBtc((priceBtc[1]));
				if(size(priceBtc[1])>maxim)
				{
					maxim=size(priceBtc[1]);
					entries[count].setLength(maxim);
					entries[count].setLongest(priceBtc[0].replaceAll("\\s+",""));
				}
				if(!priceBtc[1].replace("\"","").replaceAll("\\s+","").replace(",","").equals("null"))
				entries[count].setFields(entries[count].getFields()+1);
				
				
				temp=scan2.nextLine();
				while(temp.trim().equals("")) // checks if the line is empty
				{ 
					temp=scan2.nextLine();
				}
				String [] volume=temp.split(":");
				entries[count].setVolume24h((volume[1]));
				if(size(volume[1])>maxim)
				{
					maxim=size(volume[1]);
					entries[count].setLength(maxim);
					entries[count].setLongest(volume[0].replaceAll("\\s+",""));
				}
				if(!volume[1].replace("\"","").replaceAll("\\s+","").replace(",","").equals("null"))
				entries[count].setFields(entries[count].getFields()+1);
				
				
				temp=scan2.nextLine();
				while(temp.trim().equals("")) // checks if the line is empty
				{ 
					temp=scan2.nextLine();
				}
				String [] market=temp.split(":");
				entries[count].setMarketCap(market[1]);
				if(size(market[1])>maxim)
				{
					maxim=size(market[1]);
					entries[count].setLength(maxim);
					entries[count].setLongest(market[0].replaceAll("\\s+",""));
				}
				if(!market[1].replace("\"","").replaceAll("\\s+","").replace(",","").equals("null"))
				entries[count].setFields(entries[count].getFields()+1);
				
				
				temp=scan2.nextLine();
				while(temp.trim().equals("")) // checks if the line is empty
				{ 
					temp=scan2.nextLine();
				}
				String [] available=temp.split(":");
				entries[count].setAvailableSup((available[1]));
				if(size(available[1])>maxim)
				{
					maxim=size(available[1]);
					entries[count].setLength(maxim);
					entries[count].setLongest(available[0].replaceAll("\\s+",""));
				}
				if(!available[1].replace("\"","").replaceAll("\\s+","").replace(",","").equals("null"))
				entries[count].setFields(entries[count].getFields()+1);
				
				
				temp=scan2.nextLine();
				while(temp.trim().equals("")) // checks if the line is empty
				{ 
					temp=scan2.nextLine();
				}
				String [] total=temp.split(":");
				entries[count].setTotalSup((total[1]));
				if(size(total[1])>maxim)
				{
					maxim=size(total[1]);
					entries[count].setLength(maxim);
					entries[count].setLongest(total[0].replaceAll("\\s+",""));
				}
				if(!total[1].replace("\"","").replaceAll("\\s+","").replace(",","").equals("null"))
				entries[count].setFields(entries[count].getFields()+1);
				
				
				temp=scan2.nextLine();
				while(temp.trim().equals("")) // checks if the line is empty
				{ 
					temp=scan2.nextLine();
				}
				String [] max=temp.split(":");
				entries[count].setMaxSup((max[1]));
				if(size(max[1])>maxim)
				{
					maxim=size(max[1]);
					entries[count].setLength(maxim);
					entries[count].setLongest(max[0].replaceAll("\\s+",""));
				}
				if(!max[1].replace("\"","").replaceAll("\\s+","").replace(",","").equals("null"))
					entries[count].setFields(entries[count].getFields()+1);
				
				
				
				temp=scan2.nextLine();
				while(temp.trim().equals("")) // checks if the line is empty
				{ 
					temp=scan2.nextLine();
				}
				String [] percent1=temp.split(":");
				entries[count].setPercent1((percent1[1]));
				if(size(percent1[1])>maxim)
				{
					maxim=size(percent1[1]);
					entries[count].setLength(maxim);
					entries[count].setLongest(percent1[0].replaceAll("\\s+",""));
				}
				if(!percent1[1].replace("\"","").replaceAll("\\s+","").replace(",","").equals("null"))
				entries[count].setFields(entries[count].getFields()+1);
				
				
				temp=scan2.nextLine();
				while(temp.trim().equals("")) // checks if the line is empty
				{ 
					temp=scan2.nextLine();
				}
				String [] percent24=temp.split(":");
				entries[count].setPercent24((percent24[1]));
				if(size(percent24[1])>maxim)
				{
					maxim=size(percent24[1]);
					entries[count].setLength(maxim);
					entries[count].setLongest(percent24[0].replaceAll("\\s+",""));
				}
				if(!percent24[1].replace("\"","").replaceAll("\\s+","").replace(",","").equals("null"))
				entries[count].setFields(entries[count].getFields()+1);
				
				temp=scan2.nextLine();
				while(temp.trim().equals("")) // checks if the line is empty
				{ 
					temp=scan2.nextLine();
				}
				String [] percent7=temp.split(":");
				entries[count].setPercent7d(percent7[1]);
				if(size(percent7[1])>maxim)
				{
					maxim=size(percent7[1]);
					entries[count].setLength(maxim);
					entries[count].setLongest(percent7[0].replaceAll("\\s+",""));
				}
				if(!percent7[1].replace("\"","").replaceAll("\\s+","").replace(",","").equals("null"))
				entries[count].setFields(entries[count].getFields()+1);
				
				
				temp=scan2.nextLine();
				while(temp.trim().equals("")) // checks if the line is empty
				{ 
					temp=scan2.nextLine();
				}
				String [] update=temp.split(":"); 
		        entries[count].setLastUpdated(update[1]);
				if(size(update[1])>maxim)
				{
					maxim=size(update[1]);
					entries[count].setLength(maxim);
					entries[count].setLongest(update[0].replaceAll("\\s+",""));
				}
				if(!update[1].replace("\"","").replaceAll("\\s+","").replace(",","").equals("null"))
				entries[count].setFields(entries[count].getFields()+1);
				
				try
				{
					float lp=lossProfit(entries,count);
					entries[count].setLossP(lp);
					if(entries[count].lastUpdated.replace("\"","").replaceAll("\\s+","").replace(",","").length()==10)
					{int d=Integer.valueOf(entries[count].lastUpdated.replace("\"","").replaceAll("\\s+","").replace(",",""));
					String date=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(d * 1000L));
					entries[count].setLastUpdated(date);}
				}catch(NumberFormatException e)
					{
						System.out.println("Wrong format.Try again!");
					}	
				
				if(comp.toLowerCase().equals("n"))
				{
					new_Node=list.newNode(entries[count],entries[count].name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase(),entries[count].name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
					list.sortedInsert(new_Node);
				}
				else
					if(comp.toLowerCase().equals("sy"))
					{
						new_Node=list.newNode(entries[count],entries[count].symbol.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase(),entries[count].name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
						list.sortedInsert(new_Node);
					}
					else
						if(comp.toLowerCase().equals("m"))
						{
							if(!entries[count].maxSup.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase().equals("null"))
							{
							new_Node=list.newNode2(entries[count],Float.parseFloat(entries[count].maxSup.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase()),entries[count].name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
							list.sortedInsert(new_Node);
							}
							else
							{
								new_Node=list.newNode(entries[count],entries[count].maxSup.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase(),entries[count].name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
								list.addEnd(new_Node);
							}
						}
				count++;
				maxim=0;
				if(entries.length== count)  // checks if the array is full
				{
					doubleArray();   // doubles the array
					for(int i=count;i<entries.length;i++)
						entries[i]=new Entry();
				}
			}
			
			
		}
		
		scan2.close();
	}
	
	/**
	 * @param count the count to set
	 */
	public static void setCount(int count) {
		Reading.count = count;
	}
	/**
	 * @param entries the entries to set
	 */
	public static void setEntries(Entry[] entries) {
		Reading.entries = entries;
	}
	/**
	 * double the size of the array
	 */
	private static void doubleArray() {
		Entry[] temp=  new Entry[2*entries.length];
		for (int i = 0; i < entries.length; i++) {
			temp[i] = entries[i];
		}
		entries = temp;
		
	}
	/**
	 * Returns a clone of the entries
	 * @return the entries 
	 */
	public static Entry[] getEntries() {
		return (Entry[]) entries.clone();
	}
	/**
	 * Returns the double linked list
	 * @return the list 
	 */
	public static MyDoubleLinkedList getList()
	{
		return list;
	}
	/**
	 * Returns the number of entries
	 * @return count 
	 */
	public static int getCount()
	{
		return count;
	}
	
	/**
	 * Adds element
	 * @param element
	 */
	public static void add(Entry element) { 
		if (contains(element)) { 
			return;
		}
		
		if (entries.length == count) {
			doubleArray();
		}
		int i =  count;
		int first = 0;
		
		while ((i >  first) && (element.compareTo(entries[i -1]) < 0)) { 
			entries[i] = entries[i-1]; 
			i = i - 1;
		}
		entries[i] = element;
		count++;
	
	}

	/**
	 * Checks if the array contains the target
	 * @param target
	 * @return true/false
	 */
	public static boolean contains(Entry target) {
		// TODO Auto-generated method stub
		if (binarySearch(target) == -1)
			return false;
		else {
			return true;
		}
	}
	
	
	/**
	 * Returns the index of the target from entries
	 * @param target
	 * @return index
	 */
	private static int binarySearch(Entry target) { 
		
		int low = 0, middle, high = count -1;
		while(low < high) {
			middle = (high + low) / 2;
			if (target.compareTo(entries[middle]) == 0) {
				return middle;
			}else if (target.compareTo(entries[middle]) > 0) {
				low =  middle + 1;
			}else {
				high =  middle - 1;
			}
		}
		return -1;
	}
	
	
	/**
	 * Removes a target 
	 * @param target
	 * @return the modified array
	 */
	public static Entry remove(Entry target) {
		int index  = binarySearch(target);
		if (index  ==  -1) {
			throw new NoSuchElementException();
		}
		int i = index;
		Entry toReturn =  entries[index];
		while(i < count) {
			entries[i] = entries[i+1];
			i++;
		}
		count--;
		if ((count + 3) ==  (entries.length) /2) {
			shrinkArray();
		}
		return toReturn;
		
	}
	/**
	 * Shrinks the array
	 */
	protected static void shrinkArray() { 
		Entry [] temp = new Entry[(entries.length) /2];
		for (int i = 0; i < entries.length; i++) {
			temp[i] = entries[i];
		}
		entries = temp;
	}
	/**
	 * Removes a random target
	 * @return the modified array
	 */
	public static Entry removeRandom() { 
		if(count==0)
			return null;
		Random rand = new Random();
		int  n = rand.nextInt(count);
		Entry target=entries[n];
		for (int i = 0; i < count; i++) {
			if (target.equals(entries[i])) {
				Entry toReturn = entries[i];
				entries[i] = entries[count-1];
				count --;
				return toReturn;
			}
		}
		return null;
	}
	/** 
	 * Checks if array is empty
	 * @return true/false
	 */
	public boolean isEmpty()
	{
		return count==0;
	}
	
}

