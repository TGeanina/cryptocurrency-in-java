import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Scanner;

/**
 * @author Geanina Tambaliuc
 *
 */
public class Writing extends Reading {

	/**
	 * Writes in the file specified by the user
	 * @param i
	 * @param filename
	 * @throws NegativeNumberException
	 * @throws DivisionBy0
	 * @throws FileNotFoundException
	 */
	public static void writing( Node head,String filename) throws NegativeNumberException, DivisionBy0, FileNotFoundException
	{
		// Write the result in the file chose by the user
		PrintWriter myWriter=new PrintWriter(filename);
		int j=0;
		myWriter.println("[");
		        Node temp = head;
		        while (temp != null)
		        {
		          myWriter.println("    {");
		           myWriter.println(temp.data);
		           myWriter.println("    },");
		           temp = temp.next;
		        }

		myWriter.println("]");
		myWriter.close();
	}
}
