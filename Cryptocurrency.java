/**
 * @author Geanina Tambaliuc
 *
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Scanner;

/**
 * @author Geanina Tambaliuc
 *
 */
public class Cryptocurrency{

	
	 /**
	  * Provides a menu for the user
	 * @return the user's input
	 */
	public static String menu()
	    {
		System.out.println("How do you want to sort the entries? ");
		System.out.println("Enter N for name order.");
		System.out.println("Enter SY for symbol order.");
		System.out.println("Enter M for max_supply order.");
		System.out.print("Enter your choice: ");
		Scanner comparator=new Scanner(System.in);
		return comparator.nextLine();
	        
	    }
	 /**
	  * Provides a menu for the user
	 * @return the user's input
	 */
	public static String menu2()
	{
		System.out.println("Enter S for search");
        System.out.println("Enter D for delete");
        System.out.println("Enter W for print");
        System.out.println("Enter M for merge");
        System.out.println("Enter P for purge");
        System.out.println("Enter E for exit");
        System.out.print("Enter your choice: ");
        Scanner aScanner=new Scanner(System.in);
        return aScanner.nextLine();
	}
	 /**
	  * Checks the user's input and searches,deletes, writes, merges, purges according to that input
		 * @param args 
		 * @throws FileNotFoundException 
		 * @throws IOException
		 * @throws NegativeNumberException
		 * @throws DivisionBy0
	 * @throws ListEmptyException 
		 */
	public static void main(String[] args) throws FileNotFoundException, IOException, NegativeNumberException, DivisionBy0, ListEmptyException  {
		//reads the coins file
		
		String comparator=menu(); // the comparator provided by the user
		if(!comparator.toLowerCase().equals("n") && !comparator.toLowerCase().equals("sy") && !comparator.toLowerCase().equals("m")) // check if the input is correct
        {
            System.out.println("Invalid comparator.\n"); 
            
        }
		else
		{Reading.reading(comparator, "coins.txt"); // reads the file according to the user's comparator
		
		
		MyDoubleLinkedList list=Reading.getList(); // the sorted double linked list
		 
		String choice;
	       do
	        {
	    	    
	            choice=menu2();  //the user's choice
	            if(choice.toLowerCase().equals("s"))
	            { 
	                	 System.out.println("Enter the coin's name: ");
	                	 Scanner as=new Scanner(System.in);
		                 String n=as.nextLine();
	           
	                	 int x= list.search(n);  // search for the entry that has the name provided by the user
	                	 if(x==-1) // the coin with that name does not exists
	                	 {
	                		 System.out.println("Coin not found");
	                	 }
	                	 else
	                	 {
	                		 Node current=list.getHead();
	                		 for(int i=0;i<x-1;i++)
	                			 current=current.next;
	                		 current.displayNode(); // display the entry
	                	 }
	                
	                 
	                 
	            }
	            else if(choice.toLowerCase().equals("d"))
	            {
	            	System.out.println("Enter the coin's name: ");
	                	 
	                	 Scanner as=new Scanner(System.in);
		                 String n=as.nextLine();
	                	 
	                	 int x= list.search(n); //search for the entry to delete
	                	 if(x==-1)
	                	 {
	                		 System.out.println("Coin not found");
	                	 }
	                	 else
	                	 {
	                		 Node current=list.getHead();
	                		 for(int i=0;i<x-1;i++)
	                			 current=current.next;
	                		 current.displayNode();
	                		 System.out.println("Are you sure you want to delete this entry? Y/N :");
	                		 String confirm=as.nextLine();
	                		 if(confirm.toLowerCase().equals("n"))
	                		 {
	                			 System.out.println("The entry has not been deleted!");
	                		 }
	                		 else
	                			 if(confirm.toLowerCase().equals("y"))
	                			 {
	                				 // delete the entry and shift the elements
	                				 list.deleteSpecificNode(current);
	                				 System.out.println("The entry has been deleted!");
	                				
	                			 }
	                			 else
	                				 System.out.println("Invalid answer!");
	                	 
	                 
	                
	                	 }
	                 
	            }
	            else if(choice.toLowerCase().equals("w"))
	            {	 System.out.print("Enter the file name where you want to print: ");
	                 Scanner as3=new Scanner(System.in);
	                 String filename=as3.nextLine();
	                 
	                 File file=new File(filename);
	                 if(filename.equals(""))
	                 {
	                	 list.printList();  // print the list on the screen

	                	 System.out.println("You did not specify a name. Therefore the system wrote on the console.");
	                 }
	                 else
	                 {if(file.exists()) // check if the file exists
	                 {  // check if the user wants to overwrite the file
	                	 System.out.println("This file already exists. Do you want to overwrite it? Y/N: ");
	                	 String confirm=as3.nextLine();
                		 if(confirm.toLowerCase().equals("n"))
                		 {
                			 System.out.println("The file has not been overwrited!");
                		 }
                		 else if(confirm.toLowerCase().equals("y"))
                			 {
                				 Writing.writing(list.getHead(), filename); // writes in file
                				 System.out.println("The program wrote the file!");
                			 }
                	     else
                	      { System.out.println("Invalid answer!");}
	                 }
	                 else 
	                 {
	                	 Writing.writing(list.getHead(), filename);  // writes in file
	                	 System.out.println("The program wrote the file!");
	                 }
	                 }
	            }
	            else if(choice.toLowerCase().equals("m"))
	            {
	            	 System.out.print("Enter the file name that you want to merge: ");
	                 Scanner f=new Scanner(System.in);
	                 String filename=f.nextLine();
	                 
	                 File file=new File(filename);
	                 if(!file.exists())
	                	 System.out.println("The file does not exist");
	                 else
	                 {
	                	 Merge.reading(filename); //merge the files
	                	 MyDoubleLinkedList listM=Merge.addToLinkedlistMerge(comparator);
	                	 listM.printList();
	                	 list=listM;
	                	 System.out.println("The files have been merged!");
	                 }
	            }
	            else if(choice.toLowerCase().equals("p"))
	            {
	            	 System.out.print("Enter the file name that you want to purge: ");
	                 Scanner p=new Scanner(System.in);
	                 String filename=p.nextLine();
	                 
	                 File file=new File(filename);
	                 if(!file.exists())
	                	 System.out.println("The file does not exist");
	                 else
	                 {
	                	 Purge.reading(filename);
	                	 MyDoubleLinkedList listP=Purge.LinkedlistPurge(comparator); //purge the files
	                	 listP.printList();
	                	 list=listP;
	                	 System.out.println("The purge was executed!");
	                 }
	            }
	           else if(choice.toLowerCase().equals("e"))
	                System.out.println("Exiting program");
	            else
	                System.out.println("Invalid choice. Please reenter\n");
	        }while(!choice.toLowerCase().equals("e"));
		}
}}
