
/**
 * @author Geanina Tambaliuc
 *
 */
public class LossProfit {
	
	
	
	/**
	 * Calculate the loss/profit of an entry
	 * @param entries
	 * @param i
	 * @return loss/profit 
	 * @throws NegativeNumberException
	 * @throws DivisionBy0
	 */
	public static float lossProfit(Entry[] entries, int i) throws NegativeNumberException, DivisionBy0
	{
		float price=0,percent=0,x=0;
	    try
			{
			price=Float.parseFloat(entries[i].priceUsd.replace("\"", "").replace(",", ""));
			}
		catch(NumberFormatException e)
		{
			System.out.println("Wrong format.Field:price_usd. Try again!");
		}
		
			if(price<0)
			throw new NegativeNumberException("The input cannot be negative!");
			
			//Find the percent
			try
			{
				if(!entries[i].percent7d.replace("\"", "").replace(",", "").replaceAll("\\s+","").equals("null"))
				percent=Float.parseFloat(entries[i].percent7d.replace("\"", "").replace(",", "").replaceAll("\\s+",""));
			else if(!entries[i].percent24.replace("\"", "").replace(",", "").replaceAll("\\s+","").equals("null") && 
					entries[i].percent7d.replace("\"", "").replace(",", "").replaceAll("\\s+","").equals("null") )
				percent=Float.parseFloat(entries[i].percent24.replace("\"", "").replace(",", "").replaceAll("\\s+",""));
			else if(!entries[i].percent1.replace("\"", "").replace(",", "").replaceAll("\\s+","").equals("null") && 
					entries[i].percent24.replace("\"", "").replace(",", "").replaceAll("\\s+","").equals("null") && 
					entries[i].percent7d.replace("\"", "").replace(",", "").replaceAll("\\s+","").equals("null"))
				percent=Float.parseFloat(entries[i].percent1.replace("\"", "").replace(",", "").replaceAll("\\s+",""));
			else
					percent=0;
			}catch(NumberFormatException e)
			{
				System.out.println("Wrong format.Check the percentage fields!");
			}	
				
		if(percent==100)
			throw new DivisionBy0("The percent cannot be 100. Result: Division by 0");
		else 
			{
			x=100*price/(100+percent);
			return price-x;
			}
	}
	
	/**
	 * @param s
	 * @return the size of a String
	 */
	public static int size(String s)
	{
		s=s.replace("\"","").replaceAll("\\s+","").replace(",","");
		return s.length();
	}
	
	
	

	
}
