/** Exception for a wrong input*/
public class DivisionBy0 extends Exception {
    public DivisionBy0(String message)
    {
        super(message);
    }
    public String printStatement()
    {
       String s="The percent cannot be 100. Result: Division by 0";
       return s;
    }
}