import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Scanner;

public class Merge extends Reading {

	protected static int count2;
	protected static Entry[] entries2;
	protected static Entry[] entriesRead=Reading.getEntries();
	protected static MyDoubleLinkedList listMerge=new MyDoubleLinkedList();
	
	/**
	 * Reads the file and stores the new entries
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws NegativeNumberException
	 * @throws DivisionBy0
	 */
	public static void reading(String filename) throws FileNotFoundException, IOException, NegativeNumberException, DivisionBy0
	{
		
		
		Scanner scan2=new Scanner(new File(filename)); // reads the merge file
	    
		// create an array of objects, type Entry
		entries2= new Entry[5];
		for(int i=0;i<entries2.length;i++)
		{
			entries2[i]=new Entry(); //add element in the array
		}
		String temp;
		count2=0;
		int maxim=0;
		while(scan2.hasNext())
		{
			
			if(scan2.nextLine().equals("    {"))
			{
				temp=scan2.nextLine();		// scan file line
				while(temp.trim().equals("")) // checks if the line is empty
				{ 
					temp=scan2.nextLine();
				}
				String [] id=temp.split(":"); // splits the line in 2 sections
				entries2[count2].setId(id[1]);	
				if(size(id[1])>maxim)   // calculate the maximum field
				{
					
					maxim=size(id[1]);
					entries2[count2].setLength(maxim);
					entries2[count2].setLongest(id[0].replaceAll("\\s+",""));
				}
				if(!id[1].replace("\"","").replaceAll("\\s+","").replace(",","").equals("null") ) // count2 fields
					entries2[count2].setFields(entries2[count2].getFields()+1);
				
				temp=scan2.nextLine();
				while(temp.trim().equals("")) // checks if the line is empty
				{ 
					temp=scan2.nextLine();
				}
				String [] name=temp.split(":");
				entries2[count2].setName(name[1]);
				if(size(name[1])>maxim)
				{
					maxim=size(name[1]);
					entries2[count2].setLength(maxim);
					entries2[count2].setLongest(name[0].replaceAll("\\s+",""));
					
				}
				if(!name[1].replace("\"","").replaceAll("\\s+","").replace(",","").equals("null"))
					entries2[count2].setFields(entries2[count2].getFields()+1);
				
				
				temp=scan2.nextLine();
				while(temp.trim().equals("")) // checks if the line is empty
				{ 
					temp=scan2.nextLine();
				}
				String [] symbol=temp.split(":");
				entries2[count2].setSymbol(symbol[1]);
				if(size(symbol[1])>maxim)
				{
					maxim=size(symbol[1]);
					entries2[count2].setLength(maxim);
					entries2[count2].setLongest(symbol[0].replaceAll("\\s+",""));
				}
				if(!symbol[1].replace("\"","").replaceAll("\\s+","").replace(",","").equals("null"))
					entries2[count2].setFields(entries2[count2].getFields()+1);
				
				
				temp=scan2.nextLine();
				while(temp.trim().equals("")) // checks if the line is empty
				{ 
					temp=scan2.nextLine();
				}
				String [] rank=temp.split(":");
				entries2[count2].setRank((rank[1]));
				if(size(rank[1])>maxim)
				{
					maxim=size(rank[1]);
					entries2[count2].setLength(maxim);
					entries2[count2].setLongest(rank[0].replaceAll("\\s+",""));
				}
				if(!rank[1].replace("\"","").replaceAll("\\s+","").replace(",","").equals("null"))
					entries2[count2].setFields(entries2[count2].getFields()+1);
				
				
				temp=scan2.nextLine();
				while(temp.trim().equals("")) // checks if the line is empty
				{ 
					temp=scan2.nextLine();
				}
				String [] priceUsd=temp.split(":");
				entries2[count2].setPriceUsd((priceUsd[1]));
				if(size(priceUsd[1])>maxim)
				{
					maxim=size(priceUsd[1]);
					entries2[count2].setLength(maxim);
					entries2[count2].setLongest(priceUsd[0].replaceAll("\\s+",""));
				}
				if(!priceUsd[1].replace("\"","").replaceAll("\\s+","").replace(",","").equals("null"))
					entries2[count2].setFields(entries2[count2].getFields()+1);
				
				
				temp=scan2.nextLine();
				while(temp.trim().equals("")) // checks if the line is empty
				{ 
					temp=scan2.nextLine();
				}
				String [] priceBtc=temp.split(":");
				entries2[count2].setPriceBtc((priceBtc[1]));
				if(size(priceBtc[1])>maxim)
				{
					maxim=size(priceBtc[1]);
					entries2[count2].setLength(maxim);
					entries2[count2].setLongest(priceBtc[0].replaceAll("\\s+",""));
				}
				if(!priceBtc[1].replace("\"","").replaceAll("\\s+","").replace(",","").equals("null"))
					entries2[count2].setFields(entries2[count2].getFields()+1);
				
				
				temp=scan2.nextLine();
				while(temp.trim().equals("")) // checks if the line is empty
				{ 
					temp=scan2.nextLine();
				}
				String [] volume=temp.split(":");
				entries2[count2].setVolume24h((volume[1]));
				if(size(volume[1])>maxim)
				{
					maxim=size(volume[1]);
					entries2[count2].setLength(maxim);
					entries2[count2].setLongest(volume[0].replaceAll("\\s+",""));
				}
				if(!volume[1].replace("\"","").replaceAll("\\s+","").replace(",","").equals("null"))
					entries2[count2].setFields(entries2[count2].getFields()+1);
				
				
				temp=scan2.nextLine();
				while(temp.trim().equals("")) // checks if the line is empty
				{ 
					temp=scan2.nextLine();
				}
				String [] market=temp.split(":");
				entries2[count2].setMarketCap(market[1]);
				if(size(market[1])>maxim)
				{
					maxim=size(market[1]);
					entries2[count2].setLength(maxim);
					entries2[count2].setLongest(market[0].replaceAll("\\s+",""));
				}
				if(!market[1].replace("\"","").replaceAll("\\s+","").replace(",","").equals("null"))
					entries2[count2].setFields(entries2[count2].getFields()+1);
				
				
				temp=scan2.nextLine();
				while(temp.trim().equals("")) // checks if the line is empty
				{ 
					temp=scan2.nextLine();
				}
				String [] available=temp.split(":");
				entries2[count2].setAvailableSup((available[1]));
				if(size(available[1])>maxim)
				{
					maxim=size(available[1]);
					entries2[count2].setLength(maxim);
					entries2[count2].setLongest(available[0].replaceAll("\\s+",""));
				}
				if(!available[1].replace("\"","").replaceAll("\\s+","").replace(",","").equals("null"))
					entries2[count2].setFields(entries2[count2].getFields()+1);
				
				
				temp=scan2.nextLine();
				while(temp.trim().equals("")) // checks if the line is empty
				{ 
					temp=scan2.nextLine();
				}
				String [] total=temp.split(":");
				entries2[count2].setTotalSup((total[1]));
				if(size(total[1])>maxim)
				{
					maxim=size(total[1]);
					entries2[count2].setLength(maxim);
					entries2[count2].setLongest(total[0].replaceAll("\\s+",""));
				}
				if(!total[1].replace("\"","").replaceAll("\\s+","").replace(",","").equals("null"))
					entries2[count2].setFields(entries2[count2].getFields()+1);
				
				
				temp=scan2.nextLine();
				while(temp.trim().equals("")) // checks if the line is empty
				{ 
					temp=scan2.nextLine();
				}
				String [] max=temp.split(":");
				entries2[count2].setMaxSup((max[1]));
				if(size(max[1])>maxim)
				{
					maxim=size(max[1]);
					entries2[count2].setLength(maxim);
					entries2[count2].setLongest(max[0].replaceAll("\\s+",""));
				}
				if(!max[1].replace("\"","").replaceAll("\\s+","").replace(",","").equals("null"))
					entries2[count2].setFields(entries2[count2].getFields()+1);
				
				
				
				temp=scan2.nextLine();
				while(temp.trim().equals("")) // checks if the line is empty
				{ 
					temp=scan2.nextLine();
				}
				String [] percent1=temp.split(":");
				entries2[count2].setPercent1((percent1[1]));
				if(size(percent1[1])>maxim)
				{
					maxim=size(percent1[1]);
					entries2[count2].setLength(maxim);
					entries2[count2].setLongest(percent1[0].replaceAll("\\s+",""));
				}
				if(!percent1[1].replace("\"","").replaceAll("\\s+","").replace(",","").equals("null"))
					entries2[count2].setFields(entries2[count2].getFields()+1);
				
				
				temp=scan2.nextLine();
				while(temp.trim().equals("")) // checks if the line is empty
				{ 
					temp=scan2.nextLine();
				}
				String [] percent24=temp.split(":");
				entries2[count2].setPercent24((percent24[1]));
				if(size(percent24[1])>maxim)
				{
					maxim=size(percent24[1]);
					entries2[count2].setLength(maxim);
					entries2[count2].setLongest(percent24[0].replaceAll("\\s+",""));
				}
				if(!percent24[1].replace("\"","").replaceAll("\\s+","").replace(",","").equals("null"))
					entries2[count2].setFields(entries2[count2].getFields()+1);
				
				temp=scan2.nextLine();
				while(temp.trim().equals("")) // checks if the line is empty
				{ 
					temp=scan2.nextLine();
				}
				String [] percent7=temp.split(":");
				entries2[count2].setPercent7d(percent7[1]);
				if(size(percent7[1])>maxim)
				{
					maxim=size(percent7[1]);
					entries2[count2].setLength(maxim);
					entries2[count2].setLongest(percent7[0].replaceAll("\\s+",""));
				}
				if(!percent7[1].replace("\"","").replaceAll("\\s+","").replace(",","").equals("null"))
				entries2[count2].setFields(entries2[count2].getFields()+1);
				
				
				temp=scan2.nextLine();
				while(temp.trim().equals("")) // checks if the line is empty
				{ 
					temp=scan2.nextLine();
				}
				String [] update=temp.split(":"); 
		        entries2[count2].setLastUpdated(update[1]);
				if(size(update[1])>maxim)
				{
					maxim=size(update[1]);
					entries2[count2].setLength(maxim);
					entries2[count2].setLongest(update[0].replaceAll("\\s+",""));
				}
				if(!update[1].replace("\"","").replaceAll("\\s+","").replace(",","").equals("null"))
				entries2[count2].setFields(entries2[count2].getFields()+1);
				

				try
				{
					float lp=lossProfit(entries2,count2);
					entries2[count2].setLossP(lp);
					if(entries2[count2].lastUpdated.replace("\"","").replaceAll("\\s+","").replace(",","").length()==10)
					{int d=Integer.valueOf(entries2[count2].lastUpdated.replace("\"","").replaceAll("\\s+","").replace(",",""));
					String date=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(d * 1000L));
					entries2[count2].setLastUpdated(date);}
				}catch(NumberFormatException e)
					{
						System.out.println("Wrong format.Try again!");
					}	
				count2++;
				maxim=0;
				if(entries2.length== count2)  // checks if the array is full
				{
					doubleArray();   // doubles the array
					for(int i=count2;i<entries2.length;i++)
						entries2[i]=new Entry();
				}
			}
			
			
		}
		scan2.close();
	}
	
	/**
	 * Returns the number of entries from the merge file
	 * @return count2
	 */
	private static int getSizeMerge()
	{
		return count2;
	}
	
	/**
	 * Returns the merged list
	 * @param comp
	 * @return listMerge
	 */
	public static MyDoubleLinkedList addToLinkedlistMerge(String comp)
	{
		Node new_Node2,new_Node;
		deleteDuplicates(); // delete any duplicates
		
		//add entries from the merge file to the merged list
		for(int i=0;i<count2;i++)
		{if(comp.toLowerCase().equals("n"))
		{
			new_Node2=listMerge.newNode(entries2[i],entries2[i].name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase(),entries2[i].name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
			listMerge.sortedInsert(new_Node2);
		}
		else
			if(comp.toLowerCase().equals("sy"))
			{
				new_Node2=listMerge.newNode(entries2[i],entries2[i].symbol.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase(),entries2[i].name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
				listMerge.sortedInsert(new_Node2);
			}
			else
				if(comp.toLowerCase().equals("m"))
				{
					if(!entries2[count2].maxSup.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase().equals("null"))
					{
					new_Node2=listMerge.newNode2(entries2[i],Float.parseFloat(entries2[i].maxSup.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase()),entries2[i].name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
					listMerge.sortedInsert(new_Node2);
					}
					else
					{
						new_Node2=listMerge.newNode(entries2[i],entries2[i].maxSup.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase(),entries2[i].name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
						listMerge.addEnd(new_Node2);
					}
				}}
		
		//add entries from the original list to the merged list
		for(int i=0;i<count;i++)
		{if(comp.toLowerCase().equals("n"))
		{
			new_Node=listMerge.newNode(entriesRead[i],entriesRead[i].name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase(),entries2[i].name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
			listMerge.sortedInsert(new_Node);
		}
		else
			if(comp.toLowerCase().equals("sy"))
			{
				new_Node=listMerge.newNode(entriesRead[i],entriesRead[i].symbol.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase(),entries2[i].name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
				listMerge.sortedInsert(new_Node);
			}
			else
				if(comp.toLowerCase().equals("m"))
				{
					if(!entries[count].maxSup.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase().equals("null"))
					{
					new_Node=listMerge.newNode2(entriesRead[i],Float.parseFloat(entriesRead[i].maxSup.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase()),entriesRead[i].name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
					listMerge.sortedInsert(new_Node);
					}
					else
					{
						new_Node=listMerge.newNode(entriesRead[i],entriesRead[i].maxSup.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase(),entriesRead[i].name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
						listMerge.addEnd(new_Node);
					}
				}}
		return listMerge;
	}
	/**
	 * double the size of the array
	 */
	private static void doubleArray() {
		Entry[] temp=  new Entry[2*entries2.length];
		for (int i = 0; i < entries2.length; i++) {
			temp[i] = entries2[i];
		}
		entries2 = temp;
		
	}
	
	/**
	 * Delete duplicated entries. Keep the most recent one.
	 */
	private static void deleteDuplicates()
	{
		for(int i=0;i<count2;i++)
		{
		  for(int j=0;j<count;j++)
		  {
			  if(entries2[i].name.equals(entriesRead[j].name)) // if two name entries coincide then delete the oldest one
			  {
				
				  if(entries2[i].lastUpdated.compareTo(entriesRead[j].lastUpdated)<=0)  
				  {
					  entries2[i]=entries2[i+1];
					  count2--;
				  }
				  else
					  if(entries2[i].lastUpdated.compareTo(entriesRead[j].lastUpdated)>0)
					  {
						  entriesRead[j]=entriesRead[j+1];
						  count--;
					  }
			  }
		  }
		}
	}
}

