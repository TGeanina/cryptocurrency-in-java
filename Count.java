/**
 * @author Geanina Tambaliuc
 * 
 *
 */
public class Count {

	int fields;
	float lossP;
	String longest;
	int length;
	float profit;
	
	public Count()
	{
		
	}
	/**
	 * @param fields
	 * @param lossP
	 * @param longest
	 * @param length
	 * @param profit
	 */
	public Count(int fields, float lossP, String longest, int length, float profit) {
		this.fields = fields;
		this.lossP = lossP;
		this.longest = longest;
		this.length = length;
		this.profit = profit;
	}
	/**
	 * @return the profit
	 */
	public float getProfit() {
		return profit;
	}
	/**
	 * @param profit the profit to set
	 */
	public void setProfit(float profit) {
		this.profit = profit;
	}
	/**
	 * @return the fields
	 */
	public int getFields() {
		return fields;
	}
	/**
	 * @param fields the fields to set
	 */
	public void setFields(int fields) {
		this.fields = fields;
	}
	/**
	 * @return the loss
	 */
	public float getLossP() {
		return lossP;
	}
	/**
	 * @param lossP the loss to set
	 */
	public void setLossP(float lossP) {
		this.lossP = lossP;
	}
	/**
	 * @return the longest
	 */
	public String getLongest() {
		return longest;
	}
	/**
	 * @param longest the longest to set
	 */
	public void setLongest(String longest) {
		this.longest = longest;
	}
	/**
	 * @return the length
	 */
	public int getLength() {
		return length;
	}
	/**
	 * @param length the length to set
	 */
	public void setLength(int length) {
		this.length = length;
	}
	
	/**
	 * @return str which is the "count" string
	 */
	public String countString() {
		String str="        \"counts\":  \"fields=" +this.fields+"\"";
		if(this.lossP>0)
			str=str+", \"profit="+this.lossP+"\",";
		else if(this.lossP<0)
			str=str+", \"loss="+-this.lossP+"\",";
		else str=str+", \"loss/profit=null\""+"\",";
		str=str+" \"longest="+this.longest+ ", \"length="+this.length+"\""+","+"\n";
		return str;
			

	}
	
	/**
	 * @return fields - the size of an entry
	 */
	public int sizeEntry()
	{
		return fields;
	}

	
}
