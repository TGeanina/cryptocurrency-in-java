import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Scanner;

/**
 * @author Geanina Tambaliuc
 * Class for purge
 *
 */
public class Purge extends Reading {
	protected static int count3;
	protected static Entry[] entries3;
	protected static Entry[] entriesRead=Reading.getEntries();
	protected static MyDoubleLinkedList listPurge=new MyDoubleLinkedList();
	
	/**
	 * Reads the file and stores the entries from the purge file
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws NegativeNumberException
	 * @throws DivisionBy0
	 */
	public static void reading(String filename) throws FileNotFoundException, IOException, NegativeNumberException, DivisionBy0
	{
		
		
		Scanner scan2=new Scanner(new File(filename)); // reads the file to purge
	    
		// create an array of objects, type Entry
		entries3= new Entry[5];
		for(int i=0;i<entries3.length;i++)
		{
			entries3[i]=new Entry(); //add element in the array
		}
		String temp;
		count3=0;
		int maxim=0;
		while(scan2.hasNext())
		{
			
			if(scan2.nextLine().equals("    {"))
			{
				temp=scan2.nextLine();		// scan file line
				while(temp.trim().equals("")) // checks if the line is empty
				{ 
					temp=scan2.nextLine();
				}
				String [] id=temp.split(":"); // splits the line in 2 sections
				entries3[count3].setId(id[1]);	
				if(size(id[1])>maxim)   // calculate the maximum field
				{
					
					maxim=size(id[1]);
					entries3[count3].setLength(maxim);
					entries3[count3].setLongest(id[0].replaceAll("\\s+",""));
				}
				if(!id[1].replace("\"","").replaceAll("\\s+","").replace(",","").equals("null") ) // count3 fields
					entries3[count3].setFields(entries3[count3].getFields()+1);
				
				temp=scan2.nextLine();
				while(temp.trim().equals("")) // checks if the line is empty
				{ 
					temp=scan2.nextLine();
				}
				String [] name=temp.split(":");
				entries3[count3].setName(name[1]);
				if(size(name[1])>maxim)
				{
					maxim=size(name[1]);
					entries3[count3].setLength(maxim);
					entries3[count3].setLongest(name[0].replaceAll("\\s+",""));
					
				}
				if(!name[1].replace("\"","").replaceAll("\\s+","").replace(",","").equals("null"))
					entries3[count3].setFields(entries3[count3].getFields()+1);
				
				
				temp=scan2.nextLine();
				while(temp.trim().equals("")) // checks if the line is empty
				{ 
					temp=scan2.nextLine();
				}
				String [] symbol=temp.split(":");
				entries3[count3].setSymbol(symbol[1]);
				if(size(symbol[1])>maxim)
				{
					maxim=size(symbol[1]);
					entries3[count3].setLength(maxim);
					entries3[count3].setLongest(symbol[0].replaceAll("\\s+",""));
				}
				if(!symbol[1].replace("\"","").replaceAll("\\s+","").replace(",","").equals("null"))
					entries3[count3].setFields(entries3[count3].getFields()+1);
				
				
				temp=scan2.nextLine();
				while(temp.trim().equals("")) // checks if the line is empty
				{ 
					temp=scan2.nextLine();
				}
				String [] rank=temp.split(":");
				entries3[count3].setRank((rank[1]));
				if(size(rank[1])>maxim)
				{
					maxim=size(rank[1]);
					entries3[count3].setLength(maxim);
					entries3[count3].setLongest(rank[0].replaceAll("\\s+",""));
				}
				if(!rank[1].replace("\"","").replaceAll("\\s+","").replace(",","").equals("null"))
					entries3[count3].setFields(entries3[count3].getFields()+1);
				
				
				temp=scan2.nextLine();
				while(temp.trim().equals("")) // checks if the line is empty
				{ 
					temp=scan2.nextLine();
				}
				String [] priceUsd=temp.split(":");
				entries3[count3].setPriceUsd((priceUsd[1]));
				if(size(priceUsd[1])>maxim)
				{
					maxim=size(priceUsd[1]);
					entries3[count3].setLength(maxim);
					entries3[count3].setLongest(priceUsd[0].replaceAll("\\s+",""));
				}
				if(!priceUsd[1].replace("\"","").replaceAll("\\s+","").replace(",","").equals("null"))
					entries3[count3].setFields(entries3[count3].getFields()+1);
				
				
				temp=scan2.nextLine();
				while(temp.trim().equals("")) // checks if the line is empty
				{ 
					temp=scan2.nextLine();
				}
				String [] priceBtc=temp.split(":");
				entries3[count3].setPriceBtc((priceBtc[1]));
				if(size(priceBtc[1])>maxim)
				{
					maxim=size(priceBtc[1]);
					entries3[count3].setLength(maxim);
					entries3[count3].setLongest(priceBtc[0].replaceAll("\\s+",""));
				}
				if(!priceBtc[1].replace("\"","").replaceAll("\\s+","").replace(",","").equals("null"))
					entries3[count3].setFields(entries3[count3].getFields()+1);
				
				
				temp=scan2.nextLine();
				while(temp.trim().equals("")) // checks if the line is empty
				{ 
					temp=scan2.nextLine();
				}
				String [] volume=temp.split(":");
				entries3[count3].setVolume24h((volume[1]));
				if(size(volume[1])>maxim)
				{
					maxim=size(volume[1]);
					entries3[count3].setLength(maxim);
					entries3[count3].setLongest(volume[0].replaceAll("\\s+",""));
				}
				if(!volume[1].replace("\"","").replaceAll("\\s+","").replace(",","").equals("null"))
					entries3[count3].setFields(entries3[count3].getFields()+1);
				
				
				temp=scan2.nextLine();
				while(temp.trim().equals("")) // checks if the line is empty
				{ 
					temp=scan2.nextLine();
				}
				String [] market=temp.split(":");
				entries3[count3].setMarketCap(market[1]);
				if(size(market[1])>maxim)
				{
					maxim=size(market[1]);
					entries3[count3].setLength(maxim);
					entries3[count3].setLongest(market[0].replaceAll("\\s+",""));
				}
				if(!market[1].replace("\"","").replaceAll("\\s+","").replace(",","").equals("null"))
					entries3[count3].setFields(entries3[count3].getFields()+1);
				
				
				temp=scan2.nextLine();
				while(temp.trim().equals("")) // checks if the line is empty
				{ 
					temp=scan2.nextLine();
				}
				String [] available=temp.split(":");
				entries3[count3].setAvailableSup((available[1]));
				if(size(available[1])>maxim)
				{
					maxim=size(available[1]);
					entries3[count3].setLength(maxim);
					entries3[count3].setLongest(available[0].replaceAll("\\s+",""));
				}
				if(!available[1].replace("\"","").replaceAll("\\s+","").replace(",","").equals("null"))
					entries3[count3].setFields(entries3[count3].getFields()+1);
				
				
				temp=scan2.nextLine();
				while(temp.trim().equals("")) // checks if the line is empty
				{ 
					temp=scan2.nextLine();
				}
				String [] total=temp.split(":");
				entries3[count3].setTotalSup((total[1]));
				if(size(total[1])>maxim)
				{
					maxim=size(total[1]);
					entries3[count3].setLength(maxim);
					entries3[count3].setLongest(total[0].replaceAll("\\s+",""));
				}
				if(!total[1].replace("\"","").replaceAll("\\s+","").replace(",","").equals("null"))
					entries3[count3].setFields(entries3[count3].getFields()+1);
				
				
				temp=scan2.nextLine();
				while(temp.trim().equals("")) // checks if the line is empty
				{ 
					temp=scan2.nextLine();
				}
				String [] max=temp.split(":");
				entries3[count3].setMaxSup((max[1]));
				if(size(max[1])>maxim)
				{
					maxim=size(max[1]);
					entries3[count3].setLength(maxim);
					entries3[count3].setLongest(max[0].replaceAll("\\s+",""));
				}
				if(!max[1].replace("\"","").replaceAll("\\s+","").replace(",","").equals("null"))
					entries3[count3].setFields(entries3[count3].getFields()+1);
				
				
				
				temp=scan2.nextLine();
				while(temp.trim().equals("")) // checks if the line is empty
				{ 
					temp=scan2.nextLine();
				}
				String [] percent1=temp.split(":");
				entries3[count3].setPercent1((percent1[1]));
				if(size(percent1[1])>maxim)
				{
					maxim=size(percent1[1]);
					entries3[count3].setLength(maxim);
					entries3[count3].setLongest(percent1[0].replaceAll("\\s+",""));
				}
				if(!percent1[1].replace("\"","").replaceAll("\\s+","").replace(",","").equals("null"))
					entries3[count3].setFields(entries3[count3].getFields()+1);
				
				
				temp=scan2.nextLine();
				while(temp.trim().equals("")) // checks if the line is empty
				{ 
					temp=scan2.nextLine();
				}
				String [] percent24=temp.split(":");
				entries3[count3].setPercent24((percent24[1]));
				if(size(percent24[1])>maxim)
				{
					maxim=size(percent24[1]);
					entries3[count3].setLength(maxim);
					entries3[count3].setLongest(percent24[0].replaceAll("\\s+",""));
				}
				if(!percent24[1].replace("\"","").replaceAll("\\s+","").replace(",","").equals("null"))
					entries3[count3].setFields(entries3[count3].getFields()+1);
				
				temp=scan2.nextLine();
				while(temp.trim().equals("")) // checks if the line is empty
				{ 
					temp=scan2.nextLine();
				}
				String [] percent7=temp.split(":");
				entries3[count3].setPercent7d(percent7[1]);
				if(size(percent7[1])>maxim)
				{
					maxim=size(percent7[1]);
					entries3[count3].setLength(maxim);
					entries3[count3].setLongest(percent7[0].replaceAll("\\s+",""));
				}
				if(!percent7[1].replace("\"","").replaceAll("\\s+","").replace(",","").equals("null"))
				entries3[count3].setFields(entries3[count3].getFields()+1);
				
				
				temp=scan2.nextLine();
				while(temp.trim().equals("")) // checks if the line is empty
				{ 
					temp=scan2.nextLine();
				}
				String [] update=temp.split(":"); 
		        entries3[count3].setLastUpdated(update[1]);
				if(size(update[1])>maxim)
				{
					maxim=size(update[1]);
					entries3[count3].setLength(maxim);
					entries3[count3].setLongest(update[0].replaceAll("\\s+",""));
				}
				if(!update[1].replace("\"","").replaceAll("\\s+","").replace(",","").equals("null"))
				entries3[count3].setFields(entries3[count3].getFields()+1);

				try
				{
					float lp=lossProfit(entries3,count3);
					entries3[count3].setLossP(lp);
					if(entries3[count3].lastUpdated.replace("\"","").replaceAll("\\s+","").replace(",","").length()==10)
					{int d=Integer.valueOf(entries3[count3].lastUpdated.replace("\"","").replaceAll("\\s+","").replace(",",""));
					String date=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(d * 1000L));
					entries3[count3].setLastUpdated(date);}
				}catch(NumberFormatException e)
					{
						System.out.println("Wrong format.Try again!");
					}	
				count3++;
				maxim=0;
				if(entries3.length== count3)  // checks if the array is full
				{
					doubleArray();   // doubles the array
					for(int i=count3;i<entries3.length;i++)
						entries3[i]=new Entry();
				}
			}
			
			
		}
		scan2.close();
	}
	
	/**
	 * Returns the number of entries in the purge file
	 * @return count3
	 */
	private static int getSizeMerge()
	{
		return count3;
	}
	
	
	/**
	 * Getting the purged list
	 * @param comp
	 * @return the listPurge
	 */
	public static MyDoubleLinkedList LinkedlistPurge(String comp)
	{
		Node new_Node;
		deleteDuplicates(); //delete the duplicated entries
		for(int i=0;i<count;i++)
		{if(comp.toLowerCase().equals("n"))
		{
			new_Node=listPurge.newNode(entriesRead[i],entriesRead[i].name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase(),entriesRead[i].name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
			listPurge.sortedInsert(new_Node);
		}
		else
			if(comp.toLowerCase().equals("sy"))
			{
				new_Node=listPurge.newNode(entriesRead[i],entriesRead[i].symbol.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase(),entriesRead[i].name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
				listPurge.sortedInsert(new_Node);
			}
			else
				if(comp.toLowerCase().equals("m"))
				{
					if(!entries[count].maxSup.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase().equals("null"))
					{
					new_Node=listPurge.newNode2(entriesRead[i],Float.parseFloat(entriesRead[i].maxSup.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase()),entriesRead[i].name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
					listPurge.sortedInsert(new_Node);
					}
					else
					{
						new_Node=listPurge.newNode(entriesRead[i],entriesRead[i].maxSup.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase(),entriesRead[i].name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase());
						listPurge.addEnd(new_Node);
					}
				}}
		return listPurge;
	}
	/**
	 * double the size of the array
	 */
	private static void doubleArray() {
		Entry[] temp=  new Entry[2*entries3.length];
		for (int i = 0; i < entries3.length; i++) {
			temp[i] = entries3[i];
		}
		entries3 = temp;
		
	}
	
	/**
	 * Deletes the entries with the same name
	 */
	private static void deleteDuplicates()
	{   int ok=0;
		for(int i=0;i<count3;i++)
		{
		  for(int j=0;j<count;j++)
		  {
			  if(entries3[i].name.equals(entriesRead[j].name))
			  {
				  for(int k=j;k<count;k++)
				  entriesRead[k]=entriesRead[k+1];
				  count--;
				  ok=1;
			  }
		  }
		  if(ok==0)
		  {
			  System.out.println("The entry with the name:" +entries3[i].name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase()+" could not be found in the original database");
		  }
		  else if(ok==1)
			  ok=0;
		}
	}
}
