
/**
 * @author Geanina Tambaliuc
 * The double linked list class
 *
 */
public class MyDoubleLinkedList
{
    protected Node head;
    protected Node tail;
    public int size;
 
    /**
     * Constructor for the list
     */
    public MyDoubleLinkedList()
    {
    	head = null;
        size = 0;
    }
    
    
    /**
     * Checks if list is empty
     * @return true or false
     */
    public boolean isEmpty()
    {
        return head == null;
    }
    
    
   
    /**
     * Returns the size of the list
     * @return size
     */
    public int getSize()
    {
        return size;
    }
    
    
    /**
     * Returns the head of the list
     * @return head
     */
    public Node getHead()
    {
    	return head;
    }
    
    
    /**
     * Adding elements to the list, maintaining the order
     * @param new_node
     */
    void sortedInsert(Node new_node)
    {
         Node current;
 
         // for first node
         if (head == null || head.comp.compareTo(new_node.comp) >=0 )
         {
            new_node.next = head;
            head = new_node;
         }
         else {
 
            // Search for the point where needs to insert new node
            current = head;
 
            while (current.next != null && current.next.comp.compareTo(new_node.comp) <0 )
                  current = current.next;
 
            new_node.next = current.next;
            new_node.prev=current;
            current.next = new_node;
         }
         size++;
     }
    
    

    /**
     * Adds node at the end of the list
     * @param new_node
     */
    void addEnd(Node new_node)
    {
        
        Node last = head;
        new_node.next = null;
             
        // if the list is empty, the new node becomes the head
        if(head == null)
        {
            new_node.prev = null;
            head = new_node;
            return;
        }
             
        // Search the last node
        while(last.next != null)
            last = last.next;
             
        // change the next of last node
        last.next = new_node;
             
        // last node becomes the previous node of the new node
        new_node.prev = last;
    }
 
    
 
    /**
     * Creates a new node when comparator is a String
     * @param data
     * @param comparator
     * @param name
     * @return new node
     */
    Node newNode(Object data, String comparator,String name)
    {
       Node node = new Node(data,comparator,name);
       return node;
    }
    
    
    /**
     * Creates a new node when comparator is a float
     * @param data
     * @param comparator
     * @param name
     * @return new node
     */
    Node newNode2(Object data, float comparator,String name)
    {
       Node node = new Node(data,comparator,name);
       return node;
    }
 
    
    /**
     * Prints the list
     */
    void printList()
    {
        Node temp = head;
        while (temp != null)
        {
           System.out.print(temp.data+" ");
           System.out.println();
           temp = temp.next;
        }
    }
    
    /**
     * Deletes specific node from the list
     * @throws ListEmptyException 
     */
    public void deleteSpecificNode(Node del) throws ListEmptyException{
           
           //check if the list is empty
           if(head==null){                
                  throw new ListEmptyException("LinkedList doesn't contain any Nodes.");
           }
                        
           
           Node temp=head;
           while(temp!=del){
                  if(temp.next==null){
                        System.out.println("Given node not found in the list!");
                        return;
                  }
                  temp=temp.next;   //move to next node.
           }
           
           if(temp==head && size==1){
        	   head=null;
               
           }
           else if(temp==head){
                  head=head.next;
                  head.prev=null;
           }
           else if(temp.next==null)
           {
        	   temp.prev.next=null;
           }
           else{
                  temp.prev.next=temp.next;
                  temp.next.prev=temp.prev;
           }
           size--;
    }
    
   
    /**
     * Search for a specific node by name
     * @param x - name of the node
     * @return the index where the node is situated
     */
    public int search(String x)
    {
        Node current = head;  // initialize current
        int i=1;
        while (current != null)
        {
            if (current.name.equals(x))
                return i;    //data found
            current = current.next;
            i++;
        }
        return -1;    //data not found
    }
}