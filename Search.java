

/**
 * @author Geanina Tambaliuc
 *Search an entry using binary search or linear search
 */
public class Search {
	

    /**
     * Searches an entry for a given name using a recursive binary 
     * search.  Returns the index that contains the value or -1 if the value 
     * is not found.
     * @param entry
     * @param name
     * @return index or -1
     */    
    public static int binarySearch(Entry[] entry, String name, int min, int max) {
        if (min > max) {
            return -1;
        }
        
        int mid = (max + min) / 2;
        
        if (entry[mid].name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase().equals(name.toLowerCase())) {
            return mid;
        } else if(entry[mid].name.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase().compareTo(name.toLowerCase()) > 0) {
            return binarySearch(entry, name, min, mid - 1);
        } else {
            return binarySearch(entry, name, mid + 1, max);
        }
    }
    
    /**
     * Searches an entry for a given value using linear search  
     * Returns the index that contains the value or -1 if the value is not found.
     * @param entry
     * @param symbol
     * @return i or -1
     */
    public static int linearSearch(Entry[] entry, String symbol, int count) {
        for (int i = 0; i < count; i++) {
            if (entry[i].symbol.replace("\"", "").replace(",", "").replaceAll("\\s+","").toLowerCase().equals(symbol.toLowerCase())) {
                return i;
            }
        }
        
        return -1;
    }
    
    public static int linearSearhList(MyDoubleLinkedList list, String name,int count)
    {
    	
         return -1;
    }
    
   
    }    