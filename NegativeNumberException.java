/** Exception for a wrong input*/
public class NegativeNumberException extends Exception  {

	public NegativeNumberException(String message)
    {
        super(message);
    }
    public String printStatement()
    {
       String s="The input cannot be negative!";
       return s;
    }
}
